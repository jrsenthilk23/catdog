from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from .models import Pets
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin




# class OwnerInline(admin.StackedInline):
#     model = Owner
#     can_delete = False
#     verbose_name_plural = 'owner'
#admin.site.unregister(User)

#@admin.register(User)
#class CustomUserAdmin(UserAdmin):
#    pass

# Define a new User admin
# class UserAdmin(BaseUserAdmin):
#     inlines = (OwnerInline,)



# admin.site.register(User, UserAdmin)
#user_admin_site = CustomNonAdminUser(name='customnonadminuser')

# Register your models here.
@admin.register(Pets)
class PetsAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'owner', 'birthday',)
    list_select_related = ('owner',)
    #search_fields = ('name', 'type', 'owner', 'birthday')
    list_filter = ('name','birthday','type',)
    ordering = ('name',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """Limit choices for 'picture' field to only your pictures."""
        if db_field.name == 'owner':
            if not request.user.is_superuser:
                kwargs["queryset"] = User.objects.filter(
                    username=request.user)
        return super(PetsAdmin, self).formfield_for_foreignkey(
            db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(owner=request.user)



# @admin.register(Owner)
# class OwnerAdmin(admin.ModelAdmin):
#     list_display = ('name', 'age',)
#     search_fields = ('name', 'age')
#     list_filter = ('name', 'age',)
#     ordering = ('name',)



