from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import User


class SignUpForm(UserCreationForm):
    #full_name = forms.CharField(max_length=100, help_text='Required. 100 charaters of fewer.')
    #age = forms.IntegerField(help_text='Required. Format: Please state your correct age')

    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2") #UserCreationForm.Meta.fields + ('full_name', 'age',)