from django.apps import AppConfig


class CatdogpetConfig(AppConfig):
    name = 'catdogpet'
