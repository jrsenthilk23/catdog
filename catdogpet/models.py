from django.db import models
from django.contrib.auth.models import User

class Pets(models.Model):
    PET_CHOICES = (
        ('cat', 'cat'),
        ('dog', 'dog'),
    )
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=100,choices=PET_CHOICES,blank=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    birthday = models.DateField('birthday')

