from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from catdogpet.forms import SignUpForm
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.http import HttpResponse,JsonResponse
from django.template.loader import render_to_string
from catdogpet.models import Pets,User
from django.db.models.signals import post_save
from django.dispatch import receiver

@login_required()
def home(request):
    if request.method == 'GET':
        petsowner = Pets.objects.filter(owner = request.user).values('id','name','type','owner','birthday')
        return render(request, 'home.html', {'objects': list(petsowner)})
    elif request.method == "POST":
        selectedOption = request.POST.get('selected_option')
        selectedType = request.POST.get('selected_type')
        selectedName = request.POST.get('selected_name')
        selectedBirth = request.POST.get('selected_birth')
        petsowner = request.user
        if selectedOption == "add":
            if Pets.objects.filter(owner = petsowner,name=selectedName,type = selectedType,birthday=selectedBirth).exists() is False:
                pet = Pets(owner = petsowner,name=selectedName,type = selectedType,birthday=selectedBirth)
                pet.save()
        elif selectedOption == "update":
            selectedId = request.POST.get('selected_id')
            if selectedId == "":
                print("Error")
            elif Pets.objects.filter(id=selectedId).exists() is True:
                pet = Pets.objects.get(id=selectedId)
                pet.name = selectedName
                pet.type = selectedType
                pet.birthday = selectedBirth #check_date_format(selectedBirth)
                pet.save()
        elif selectedOption == "delete":
            selectedId = request.POST.get('selected_id')
            if selectedId == "":
                print("Error")
            elif Pets.objects.filter(id=selectedId).exists() is True:
                pet = Pets.objects.get(id=selectedId)
                pet.delete()

        petsowner = Pets.objects.filter(owner=request.user).values('id', 'name', 'type', 'owner', 'birthday')
        message = render_to_string('hometable.html',{'objects': list(petsowner)})
        data = {}
        data['msg'] = message
        return JsonResponse((data))

#handle registration
def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.save()
            raw_password = form.cleaned_data.get('password1')
            owner = authenticate(username=user.username, password=raw_password)
            login(request, owner)
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', { 'form' : form })

def calendar(request):
    return render(request,'calendar.html')

#automatically add user to privilege group to access pets model
@receiver(post_save, sender=User)
def post_save_user_signal_handler(sender, instance, created, **kwargs):
    from django.contrib.auth.models import Group
    if created:
       instance.is_staff = True
       group = Group.objects.get(name='PetOwners')
       instance.groups.add(group)
       instance.save()

def check_date_format(selectedBirth):
    from datetime import datetime
    try:
        oldformat = datetime.strptime(selectedBirth, '%b. %d, %Y')
    except:
        oldformat = datetime.strptime(selectedBirth, '%B %d, %Y')

    return oldformat.strftime('%Y-%m-%d')