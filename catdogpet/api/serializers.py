from rest_framework import serializers
from catdogpet.models import User,Pets


class AttrPKField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context['request'].user
        print(user)
        queryset = User.objects.filter(username=user)
        return queryset


class PetsSerializer(serializers.HyperlinkedModelSerializer):
    owner = AttrPKField(many=False)

    class Meta:
        model = Pets
        fields = ('id', 'name', 'owner','type', 'birthday',)