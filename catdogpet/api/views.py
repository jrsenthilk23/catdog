from catdogpet.models import Pets
from rest_framework import viewsets,permissions
from catdogpet.api import permissions
from catdogpet.api.serializers import PetsSerializer



class PetsViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsOwnerOrReadOnly]
    queryset = Pets.objects.all()
    serializer_class = PetsSerializer